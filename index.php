<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale-1">
	<title>PHP SC S04 A01</title>
</head>
<body>

    <h1>Building</h1>
    <p><?php echo $building->getName(); ?></p>
    <p><?php echo $building->getFloors(); ?></p>
    <p><?php echo $building->getAddress(); ?></p>
    <?php echo $building->setName('Caswynn Complex'); ?>

	<h1>Condominium</h1>
    <p><?php echo $condominium->getName(); ?></p>
    <p><?php echo $condominium->getFloors(); ?></p>
    <p><?php echo $condominium->getAddress(); ?></p>
    <?php echo $condominium->setName('Enzo Tower'); ?>


</body>
</html>
